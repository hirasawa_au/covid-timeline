# Sila's Timeline

### 19 กุมภาพันธ์    
    - 16.00 วิ่งออกกำลังกายที่รอบมหาวิทยาลัยเกษตรศาสตร์
    - 18.00 กินข้าวกลางซอยงามวงศ์วาน 46
    - 19.00 เดินทางกลับหอพัก (ใส่แมสตลอด)
### 20 กุมภาพันธ์
    - 17.00 ไปหาเพื่อนที่หอ NS2 งามวงศ์วาน 46
    - 18.00 กิน Tokpokki ที่ Home Village Kaset
### 21 กุมภาพันธ์
    - 11.00 ไปนั่งหอสมุดเกษตร (ใส่แมสตลอด)
    - 12.00-14.00 นั่งกินข้าวที่โรงอาหารกลาง 2
    - 14.00 เดินทางไปหอเพื่อน (ใส่แมสตลอด)
    - 14.30 อยู่หอเพื่อน NS2 งามวงศ์วาน 46
    - 18.00 กินข้าวที่ครัวชมพู งามวงศ์วาน 46
    - 19.00 เดินทางกลับหอพัก (ใส่แมสตลอด)
### 22 กุมภาพันธ์
    - 08.00 นั่งเรียนที่ใต้ตึกภาควิชาวิศวกรรมคอมพิวเตอร์ (ใส่แมสตลอด)
    - 10.00 ทำบุญภาควิชาวิศวกรรมคอมพิวเตอร์ (ใส่แมสตลอด)
    - 12.00 นั่งกินขนมที่โรงอากาศคณะวิศวกรรมศาสตร์
    - 13.00 ไปหอพักเพื่อน NS2 งามวงศ์วาน 46
    - 18.00 กินซูซิบ้านไข่หวาน Home Village Kaset
    - 19.00 เดินทางกลับหอ (ใส่แมสตลอด)
### 23 กุมภาพันธ์
    - 12.30 ไปหอพักเพื่อน NS2 งามวงศ์วาน 46
    - 13.00 ไป Lab HPCNC ที่ภาควิชาวิศวกรรมคอมพิวเตอร์ (ใส่แมสตลอด)
    - 18.30 กินข้าวที่ Sam steak
    - 19.30 เดินทางกลับหอ
### 24 กุมภาพันธ์
    - 15.00 ไปจามจุรีสแวร์ สามย่าน (ใส่แมสตลอด)
    - 17.00 กินไอศกรีมที่สามย่านมิดทาวน์
    - 18.00 นั่งกินข้าวที่ดาดฟ้าสามย่านมิดทาวน์
    - 19.00 กลับหอพัก (ใส่แมสตลอด)
### 25 กุมภาพันธ์
    - 13.00 ไปรับของที่เซ็นทรัลลาดพร้าว (ใส่แมสตลอด)
    - 14.00 รู้ว่าเพื่อนที่ไปหาบ่อย ATK เป็น + / กลับหอทันที (ใส่แมสตลอด)
    - 14.40 ตรวจ ATK เป็น -
### 26 กุมภาพันธ์
    - 13.00 ตรวจ ATK เป็น -
    - 16.00 ไปปรี้นงาน HI SPEED LASER PRINT
### 27 กุมภาพันธ์
    - 18.00 ตรวจ ATK เป็น -
### 28 กุมภาพันธ์
    - 18.40 เริ่มเป็นไข้
    - 19.00 ตรวจ ATK เป็น +